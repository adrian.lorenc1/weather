<?php

namespace Drupal\weather\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class WeatherConfigForm
 *
 * @package Drupal\pega_antivirus\Form
 */
class WeatherConfigForm extends ConfigFormBase {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'weather_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['weather.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('weather.settings');

    $form['api'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API key'),
      '#default_value' => $config->get('api'),
      '#description' => t('Openweathermap.org API key.'),
      '#required' => TRUE,
    ];

    $form['city'] = [
      '#type' => 'textfield',
      '#title' => $this->t('city'),
      '#default_value' => $config->get('city'),
      '#description' => t('Default city.'),
      '#required' => TRUE,
    ];

    $form['code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('code'),
      '#default_value' => $config->get('code'),
      '#description' => t('Default country code.'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('weather.settings')
      ->set('api', $form_state->getValue('api'))
      ->set('city', $form_state->getValue('city'))
      ->set('code', $form_state->getValue('code'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
