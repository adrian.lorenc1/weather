<?php

namespace Drupal\weather\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\weather\Exception\WeatherException;
use Drupal\weather\WeatherEndpointInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class WeatherController.
 *
 * @package Drupal\weather\Controller
 */
class WeatherController extends ControllerBase {

  /**
   * Weather endpoint.
   *
   * @var \Drupal\weather\WeatherEndpointInterface
   */
  protected $weatherEndpoint;

  /**
   * WeatherController constructor.
   *
   * @param \Drupal\weather\WeatherEndpointInterface $weather_endpoint
   */
  public function __construct(WeatherEndpointInterface $weather_endpoint) {
    $this->weatherEndpoint = $weather_endpoint;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /** @var \Drupal\weather\WeatherEndpointInterface $weather_endpoint */
    $weather_endpoint = $container->get('weather.open_weather_map');

    return new static($weather_endpoint);
  }

  /**
   * Display weather.
   *
   * @param string $city
   *  City name.
   * @param string $code
   *  Country code.
   *
   * @return array
   *   Return markup array.
   */
  public function content($city = '', $code = '') {
    $markup = [];
    $markup['#cache'] = [
      'contexts' => [
        'url.path'
      ],
      'max-age' => 60
    ];

    try {
      $weather_data = $this->weatherEndpoint->getWeatherData($city, $code);
      $markup['#theme'] = 'weather';
      $markup['#weather_data'] = $weather_data;

    } catch (WeatherException $e) {
      $markup['#markup'] = '<p>' . $this->t('An error has occurred, please try again later') . '</p>';
    }

    return $markup;
  }

}
