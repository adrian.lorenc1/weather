<?php

namespace Drupal\weather;

/**
 * Interface WeatherEndpointInterface.
 *
 * @package Drupal\weather
 */
interface WeatherEndpointInterface {

  /**
   * Get weather object for a specific endpoint.
   *
   * @param string $city
   *  City to check
   * @param string $code
   *  Code to check
   *
   * @return \Drupal\weather\WeatherDataInterface
   *
   * @throws \Drupal\weather\Exception\WeatherException
   */
  public function getWeatherData(string $city, string $code): WeatherDataInterface;

}
