<?php

namespace Drupal\weather;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Url;
use Drupal\weather\Exception\WeatherException;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;

/**
 * Class OpenWeatherMapEndpoint.
 *
 * @package Drupal\weather
 */
class WeatherEndpointOpenWeatherMap implements WeatherEndpointInterface {

  /**
   * Endpoint URL.
   */
  const ENDPOINT_URL = 'https://api.openweathermap.org/data/2.5/weather';

  /**
   * The update settings
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $weatherSettings;

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * WeatherEndpointOpenWeatherMap constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   A Guzzle client object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ClientInterface $http_client) {
    $this->httpClient = $http_client;
    $this->weatherSettings = $config_factory->get('weather.settings');
  }

  /**
   * @inheritDoc
   */
  public function getWeatherData(string $city, string $code): WeatherDataInterface {

    try {
      $params = [
        'q' => $this->generateQuery($city, $code),
        'appid' => $this->weatherSettings->get('api'),
      ];

      $url = Url::fromUri(self::ENDPOINT_URL, ['query' => $params]);

      $request = $this->httpClient->get($url->toString());
      $response = json_decode($request->getBody(), TRUE);

      $data = new WeatherData();
      $data->setTemperature($response['main']['temp']);
      $data->setWeatherDescription($response['weather']['0']['description']);

    } catch (RequestException $e) {
      // TODO: Log an error to db.
      throw new WeatherException('Could not retrieve data from endpoint.');
    }

    return $data;
  }

  /**
   * Generate q param.
   *
   * @param string $city
   *  City
   * @param string $code
   *  Code
   *
   * @return string
   */
  private function generateQuery(string $city, string $code): string {
    // TODO: Do we want to validate code?

    // I assume that code and country needs to be always provided.
    if (empty($city)) {
      $q = $this->weatherSettings->get('city');
    }
    else {
      $q = $city;
    }

    $q .= ',';

    if (empty($code)) {
      $q .= $this->weatherSettings->get('code');
    }
    else {
      $q .= $code;
    }

    return $q;
  }

}
