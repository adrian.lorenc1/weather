<?php

namespace Drupal\weather\Exception;

use Exception;

/**
 * Class WeatherException.
 *
 * @package Drupal\weather\Exception
 */
class WeatherException extends Exception {

}
