<?php

namespace Drupal\weather;

/**
 * Interface WeatherInterface
 *
 * @package Drupal\weather
 */
interface WeatherDataInterface {

  /**
   * Get actual Temperature
   *
   * @return int
   *  Temperature
   *
   */
  public function getTemperature(): int;

  /**
   * Get weather description.
   *
   * @return string
   *   Weather description.
   */
  public function getWeatherDescription(): string;

  /**
   * Set temperature.
   *
   * @param int $temp
   */
  public function setTemperature(int $temp): void;

  /**
   * Set weather description.
   *
   * @param string $desc
   */
  public function setWeatherDescription(string $desc): void;

}
