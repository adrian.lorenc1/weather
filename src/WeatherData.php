<?php

namespace Drupal\weather;

/**
 * Class WeatherData
 *
 * @package Drupal\weather
 */
class WeatherData implements WeatherDataInterface {

  /**
   * Temperature.
   *
   * @var int
   */
  private $temp;

  /**
   * Weather description.
   *
   * @var string
   */
  private $desc;

  /**
   * @inheritDoc
   */
  public function getTemperature(): int {
    return $this->temp;
  }

  /**
   * @inheritDoc
   */
  public function getWeatherDescription(): string {
    return $this->desc;
  }

  /**
   * @inheritDoc
   */
  public function setTemperature(int $temp): void {
    $this->temp = $temp;
  }

  /**
   * @inheritDoc
   */
  public function setWeatherDescription(string $desc): void {
    $this->desc = $desc;
  }


}
